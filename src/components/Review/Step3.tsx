import React, { TextareaHTMLAttributes, useState } from 'react'
interface IProps {
    click() : void
}
const Step3: React.FC<IProps> = ({click}) => {

    const [text, setText] = useState('')
    // const handleChange: React.ChangeEvent<HTMLInputElement> = (e) => {
    //     setText(e.target.value)
    // }

    return(
        <div className="step3">
            <h3>Расскажите подробнее</h3>
            <p>Напишите, пожалуйста, почему вы дали такую оценку.<br/>
            Это поможет нам сделать сервис лучше</p>

            <form>
                <label>
                    Комментарий
                    <input placeholder="Введите текст..." value={text} />
                </label>
                <button onClick={click}>Отправить отзыв</button>
            </form>
        </div>
    )
}

export default Step3;