import React from 'react'
interface IProps {
    click() : void
}
const Step4: React.FC<IProps> = ({click}) => {
    React.useEffect(() => {
        setTimeout( ()=> {
            click()
        }, 2000)
    }, [])


    return(
        <div className="step4">
            <h3>Спасибо!</h3>
            <p>Мы изучим все отзывы и учтём их в дальнейшей работе.</p>
        </div>
    )
}

export default Step4;