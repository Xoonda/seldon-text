import React, {useState} from 'react'

interface IProps {
    click() : void
}

const Step2: React.FC<IProps> = ({click}) => {

    const [indexItem, setIndexItem] = useState(0)

    const arr = []
    for (let i = 1; i <= 10; i++) {
        arr.push(i)
    }
    // console.log(arr)

    const mouseHandler = (item: number) => {
        setIndexItem(item)
        // console.log()
    }

    const color = (item: number) => {
        if (indexItem >= item) {
            return 'boxes-hover review-boxes'
        } else {
            return 'review-boxes'
        }
    }
    const mouseLeave = () => {
        setIndexItem(0)
    }
    return(
        <div className="step2">
            <h3>Оцените продукт по 10-бальной шкале</h3>
            <p>Какова вероятность, что вы порекомендуете сервис друзьям, коллегам, партнёрам?</p>
        {arr.map( (item,index) => {
            return (
                <span  key={index} className={color(item)} onClick={click} onMouseLeave={mouseLeave} onMouseEnter={() => mouseHandler(item)}>
                    <span className="spanForItem">{item}</span>
                </span>
            )
        })}            
        </div>
    )
}

export default Step2;