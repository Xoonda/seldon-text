import React from 'react'
import logo from '../../images/review.svg'


interface IProps {
    click() : void
}

const Step1: React.FC<IProps> = ({click}) => {
    return (
        <div className="step1">
            <img src={logo} alt=""/>
            <div className="bottom">
                <h3>Помогите нам стать лучше</h3>
                <p>Поделитесь своими впечатлениями о работе в сервисе.<br/>
                Это займёт меньше двух минут.</p>
                <button onClick={click}>Пройти опрос</button>
            </div>
        </div>
    )
}

export default Step1;