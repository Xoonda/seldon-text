import React, { useState } from 'react'
import Step1 from './Review/Step1'
import Step2 from './Review/Step2'
import Step3 from './Review/Step3'
import Step4 from './Review/Step4'

const Wrapper: React.FC = () => {

    const [state, setState] = useState({
        vision: false,
        count: 1
    })
    React.useEffect( ()=> {
        setTimeout ( () => {
            setState ( state => ({...state, vision: true}))
        }, 1000)
    }, [])
    const handleCloser = () => {
        setState ( state => ({...state, vision: false}))
    }
    
    const handleClick = () => {
        setState( state => ({...state, count: state.count + 1}))
        if (state.count === 4) {
            handleCloser()
        }
    }

    const changeCount = () => {
        setState( state => ({...state, count: state.count + 1}))
        console.log(state.count)
        if (state.count === 4) {
            handleCloser()
        }
    }
    return (
        <>
        <button onClick={handleClick}>Click me</button>
        { state.vision && <div className="wrapper">
                <span className="closer" onClick={handleCloser}>X</span>
                
                {(state.count === 1) && <Step1 click={changeCount} />}
                {(state.count === 2) && <Step2 click={changeCount}/>}
                {(state.count === 3) && <Step3 click={changeCount}/>}
                {(state.count === 4) && <Step4 click={changeCount}/>}


            </div>}
        </>
    )
}

export default Wrapper